<?php

function read_dictionary($filename="")
{
    $dictionary_file = "{$filename}";

    return file($dictionary_file, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
}

$basic_words = read_dictionary('friendly_words.txt');

$brand_words = read_dictionary('brand_words.txt');

//could use array_unique();
$words = array_merge($brand_words, $basic_words);

echo $words[0] . "<br>";
echo $words[1] . "<br>";
echo $words[200] . "<br>";
echo $words[300] . "<br>";
echo $words[400] . "<br>";

?>
































